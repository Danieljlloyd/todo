"""
API for a todo application.
"""

import sqlite3
import json
import time
import sys
import logging

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(format=FORMAT)

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
conn = sqlite3.connect('todo.db')

class TodoModel(object):
    """
    Class responsible for the interaction of todo objects between the
    presentation layer and the data
    """

    def __init__(self):
        self.c = conn.cursor()

        self.c.execute("""
            CREATE TABLE IF NOT EXISTS Todo (
                id INTEGER PRIMARY KEY,
                last_modified INT NOT NULL,
                text VARCHAR(255) NOT NULL,
                done INT NOT NULL
            );
        """)
            
        conn.commit()

    def add_todo(self, data):
        """
        Add a new todo item to the database.
        """

        data['last_modified'] = int(time.time())

        self.c.execute("""
            INSERT INTO Todo
            (last_modified, text, done)
            VALUES (%(last_modified)s, "%(text)s", %(done)s);
        """ % data)

        conn.commit()

        self.c.execute("""
            SELECT last_insert_rowid();
        """)
        
        last_row = self.c.fetchone()[0]

        data['id'] = last_row

        return data

    def update_todo(self, data):
        """
        Update a todo that is in the database.
        """

        data['last_modified'] = int(time.time()) 

        self.c.execute("""
        UPDATE Todo
        SET last_modified = %(last_modified)s,
            text = "%(text)s",
            done = %(done)s
        WHERE id == %(id)s;
        """ % data)

        conn.commit()

        return data

    def get_todos(self, data):
        """
        Get all of the todos that are in the database.
        """

        if 'id' not in data:
            self.c.execute("""
            SELECT * FROM Todo
            WHERE done == 0
            """ % data)
        else:
            self.c.execute("""
            SELECT * FROM Todo
            WHERE id == %(id)s and
            done == 0
            """ % data)

        conn.commit()

        all_todos = self.c.fetchall()

        todos = []
        for todo in all_todos: 
            new_todo = {
                'id': todo[0],
                'last_modified': todo[1],
                'text': todo[2],
                'done': todo[3]
            }

            todos.append(new_todo)

        return todos

def application(environ, start_response):
    """
    WSGI handler for the Todo API.
    """

    todo_model = TodoModel()

    request_method = environ.get('REQUEST_METHOD', '')
    content_length = environ.get('CONTENT_LENGTH', '')
    fp = environ.get('wsgi.input', '')
    query_string = environ.get('QUERY_STRING', '')

    log.error('Request method: ' + request_method)
    log.error('Content length: ' + content_length)
    log.error('Query string: ' + query_string)

    if (request_method == 'GET'):
        query_data = json.loads(query_string)

        response = todo_model.get_todos(query_data)

    elif (request_method == 'POST'):
        post_string = fp.read(int(content_length))
        log.error('Post data: ' + str(post_string))
        post_data = json.loads(post_string)

        response = todo_model.add_todo(post_data)

    elif (request_method == 'PUT'):
        put_string = fp.read(int(content_length))
        put_data = json.loads(put_string)

        response = todo_model.update_todo(put_data)

    elif (request_method == 'OPTIONS'):
        start_response('200 OK', 
            [('Content-type', 'text/html'),
            ('Access-Control-Allow-Origin', '*'),
            ('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS')])
        return [''.encode('utf8')]

    response_str = json.dumps(response)

    log.error('Returning response: ' + response_str)

    start_response('200 OK', [('Content-type', 'text/html')])

    return [response_str.encode('utf8')]

if __name__ == '__main__':
    from wsgiref.simple_server import make_server

    httpd = make_server('', 8051, application)
    print('Serving on port 8051...')
