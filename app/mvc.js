function Controller(model, view) {
  this.model = model;
  this.view = view;
  this.view.render();

  this.add_todo = function () {
    new_text = this.view.get_text();
    this.view.clear_textbox();
    this.view.unfocus();

    this.model.add_todo(new_text);
    this.view.render();
  };

  this.remove_todo = function (id) {
    this.model.remove_todo(id);
    this.view.render();
  };
}

function Model() {
  this.todos = [];
  this.server = 'localhost';

  this.get_all_todos = function () {
    console.log('Fetching todos');

    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'http://'+this.server+':8051?' + JSON.stringify({}), false);
    xhr.setRequestHeader('Content-Type', 'text/plain');

    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          console.log(xhr.responseText);
          app.model.todos = JSON.parse(xhr.responseText);

          console.log('Retrieved todos: ');
          for (var i=0; i<app.model.todos.length; i++) {
            console.log('id: ' + app.model.todos[i].id);
            console.log('text: ' + app.model.todos[i].text);
            console.log('last_modified: ' + app.model.todos[i].last_modified);
            console.log('done: ' + app.model.todos[i].done);
          }
        }
      }

    };

    xhr.send();

    return this.todos;
  }

  this.add_todo = function (new_text) {
    new_todo = {
      "text": new_text,
      "done": 0
    };

    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'http://'+this.server+':8051', false);
    xhr.setRequestHeader('Content-type', 'text/plain');

    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          console.log('Creating new todo: ' + xhr.responseText);
        }
      }
    };

    xhr.send(JSON.stringify(new_todo));
  };

  this.remove_todo = function (id) {
    var xhr = new XMLHttpRequest();
    xhr.open('PUT', 'http://'+this.server+':8051', false);
    xhr.setRequestHeader('Content-type', 'text/plain');

    done_todo = {}
    for (var i=0; i<this.todos.length; i++) {
      if (this.todos[i].id == id) {
        done_todo = this.todos[i];
        done_todo.done = 1;
      }
    }

    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          console.log('Removing todo ' + done_todo);
        }
      }
    };

    xhr.send(JSON.stringify(done_todo));
  };
}

function View(model) {
  this.model = model;
  this.etextbox = document.getElementById('my-textbox');
  this.etodo = document.getElementById('todo-list');

  this.todo_template = document.getElementById('todo-template');
  this.callback_str = 'app.controller.remove_todo($id)';

  this.render = function () {
    var view_html = '';

    todos = this.model.get_all_todos();

    for (var i=0; i < todos.length; i++) {
      var my_todo = todos[i];

      var temp = this.todo_template.innerHTML;
      temp = temp.replace('$callback', this.callback_str);
      temp = temp.replace('$text', my_todo.text);
      temp = temp.replace(/\$id/g, my_todo.id);

      console.log(temp);
      view_html = view_html + temp;
    }

    console.log(view_html);

    this.etodo.innerHTML = view_html;
  };

  this.unfocus = function () {
    this.etextbox.blur();
  };

  this.get_text = function () {
    return this.etextbox.value;
  };

  this.clear_textbox = function () {
    this.etextbox.value = '';
  };

  this.etextbox.onkeypress = function (e) {
    if (e.keyCode === 13) {
      app.controller.add_todo();
    }
  };
}

window.onload = function () {
  app = {};
  app.model = new Model();
  app.view = new View(app.model);
  app.controller = new Controller(app.model, app.view);
}
